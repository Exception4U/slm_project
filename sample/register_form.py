from django import forms

class registration_form(forms.Form):
    member_code = forms.CharField(label="Member Code")
    f_name = forms.CharField(max_length=20, min_length=1, label="Your Name")
    m_name = forms.CharField(max_length=20, min_length=1, label="Middle Name")
    l_name = forms.CharField(max_length=20, min_length=1,label="Last Name")
    sex = forms.ChoiceField(choices=(   ("male","Male"),
                                        ("female","Female"),
                                    )
                            )
    age = forms.IntegerField(max_value=150, min_value=12)
    status = forms.ChoiceField(choices=(   ("married","Married"),
                                            ("unmarried","Unmarried"),
                                        )
                                )
    education = forms.CharField()
    present_address = forms.CharField()
    present_address_pin = forms.IntegerField()
    permonant_address = forms.CharField()
    permonant_address_pin = forms.IntegerField()
    pan_no = forms.CharField()
    passport_no = forms.CharField()
    mobile_no = forms.CharField()
    landline_no = forms.CharField()
    e_mail = forms.EmailField()
    alternate_email = forms.EmailField()
    city = forms.CharField(min_length=1)
    pin = forms.IntegerField()
    state = forms.CharField(min_length=1)
    country = forms.CharField(min_length=1)

class feedback_form(forms.Form):
    f_name = forms.CharField(max_length=20, min_length=1, label="Your Name")
    l_name = forms.CharField(max_length=20, min_length=1,label="Last Name")
    e_mail = forms.EmailField()
    mobile_no = forms.CharField()
    comments = forms.CharField(widget= forms.Textarea)
