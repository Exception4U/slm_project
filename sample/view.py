from django.shortcuts import render, render_to_response
from sample.models import feedback_model
from sample.models import registration_model
from sample.register_form import registration_form,feedback_form


def home(request):
    return render(request, "sample/homepage.html")

def demo(request):
    return render(request, "sample/demo.html")

from sample.register_form import registration_form

def register(request):
    if request.method == 'POST': # If the form has been submitted...
        # ContactForm was defined in the the previous section
        form = registration_form(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            print(form.cleaned_data)
            fields = registration_model(
                member_code= form.cleaned_data['member_code'],
                f_name= form.cleaned_data['f_name'],
                m_name= form.cleaned_data['m_name'],
                l_name= form.cleaned_data['l_name'],
                sex= form.cleaned_data['sex'],
                age= form.cleaned_data['age'],
                status= form.cleaned_data['status'],
                education= form.cleaned_data['education'],
                present_address= form.cleaned_data['present_address'],
                present_address_pin= form.cleaned_data['present_address_pin'],
                permonant_address= form.cleaned_data['permonant_address'],
                permonant_address_pin= form.cleaned_data['permonant_address_pin'],
                pan_no= form.cleaned_data['pan_no'],
                passport_no= form.cleaned_data['passport_no'],
                mobile_no= form.cleaned_data['mobile_no'],
                landline_no= form.cleaned_data['landline_no'],
                e_mail= form.cleaned_data['e_mail'],
                alternate_email= form.cleaned_data['alternate_email'],
                city= form.cleaned_data['city'],
                pin= form.cleaned_data['pin'],
                state= form.cleaned_data['state'],
                country= form.cleaned_data['country']
            )
            fields.save()
            # return HttpResponseRedirect('/home/') # Redirect after POST
    else:
        form = registration_form() # An unbound form

    return render(request, 'sample/register.html', {
        'form': form,
    })
    # return render(request,"sample/register.html")

def about_us(request):
    return render(request,"sample/about_us.html")

def feedback(request):
    if request.method == 'POST': # If the form has been submitted...
        # ContactForm was defined in the the previous section
        form = feedback_form(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            fields = feedback_model(
                                        f_name= form.cleaned_data["f_name"],
                                        l_name= form.cleaned_data["l_name"],
                                        mobile_no= form.cleaned_data["mobile_no"],
                                        e_mail= form.cleaned_data["e_mail"],
                                        comments= form.cleaned_data["comments"]
                                     )
            fields.save()
            # return HttpResponseRedirect('/home/') # Redirect after POST
    else:
        form = feedback_form() # An unbound form

    return render(request, 'sample/feedback.html', {
        'form': form,
    })
    # return render(request,"sample/feedback.html")