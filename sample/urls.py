from django.conf.urls import patterns, include, url

from django.contrib import admin
from sample.view import home,about_us,feedback,demo,register
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', home),
    # url(r'^blog/', include('blog.urls')),
#     url(r'^', home),
    url(r'^admin/$', include(admin.site.urls)),
    url(r'^home/$', home),
    url(r'^about_us/$', about_us),
    url(r'^feedback/$', feedback),
    url(r'^register/$', register),
    url(r'^demo/$', demo),
    
)
